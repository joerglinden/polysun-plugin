package com.velasolaris.plugin.controller.demo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import com.velasolaris.plugin.controller.spi.AbstractPluginController;
import com.velasolaris.plugin.controller.spi.PluginControllerConfiguration;
import com.velasolaris.plugin.controller.spi.PluginControllerConfiguration.ControlSignal;
import com.velasolaris.plugin.controller.spi.PluginControllerConfiguration.Log;
import com.velasolaris.plugin.controller.spi.PluginControllerConfiguration.Property;
import com.velasolaris.plugin.controller.spi.PluginControllerConfiguration.Sensor;
import com.velasolaris.plugin.controller.spi.PluginControllerException;
import com.velasolaris.plugin.controller.spi.PolysunSettings;

/**
 * Demo plugin controller. Demonstrating the possibilities of Polysun plugin
 * controllers.
 *
 * This class is a copy of the FlowratePluginController.
 *
 * The FlowratePluginController implementation is written in name based approach.
 * The DemoFlowratePluginController implementation is written in an array access style.
 *
 * Controls the status (on/off) of one or two different components and the
 * flowrate of a given pump based on one or two flowrate sensors.
 *
 * The DemoFlowratePluginController provides the same functionality as the
 * FlowrateController.
 *
 * The names of properties, sensors or control signals in this class are
 * internal Polysun translation keys. Normal text can also be used.
 *
 * @author rkurmann
 * @since Polysun 9.1
 *
 */
public class DemoFlowratePluginController extends AbstractPluginController {

    /** Static instance of the Logger for this class */
    protected static Logger sLog = Logger.getLogger(DemoFlowratePluginController.class.getName());

    static final int DESGIN_FLOWRATE_VARIABLE = 0;
    static final int DESGIN_FLOWRATE_FIXED = 1;

    /** Property name for number of generic sensors in the controller element GUI. */
    static final String PROPERTY_NUM_GENERIC_SENSORS = "Number of sensors";

    /** Property name for number of generic control signals in the controller element GUI. */
    static final String PROPERTY_NUM_GENERIC_CONTROL_SIGNALS = "Number of controls signals";

    /** Timestep counter. This is some state. */
    private int timestepCounter;

    /**
     * Default constructor.
     */
    public DemoFlowratePluginController() {
    }

    @Override
    public String getName() {
        return "DemoFlowrate";
    }

    @Override
    public PluginControllerConfiguration getConfiguration(Map<String, Object> parameters) {
        List<String> printMessage = new ArrayList<>();

        List<Property> properties = new ArrayList<>();
        List<Sensor> sensors = new ArrayList<>();
        sensors.add(new Sensor("Flowrate sensor 1", "l/h", true, true)); // Flowrate sensor 1
        sensors.add(new Sensor("Flowrate sensor 2", "l/h", true, false)); // Flowrate sensor 2
        sensors.add(new Sensor("Variable flowrate sensor", "l/h", true, false)); // Variable flow rate

        List<ControlSignal> controlSignals = new ArrayList<>();
        controlSignals.add(new ControlSignal("Signal pump 1", "", false, true)); // On/Off pump 1
        controlSignals.add(new ControlSignal("Signal pump 2", "", false, false)); // On/Off pump 2
        controlSignals.add(new ControlSignal("Flowrate pump 1", "l/h", true, false)); // Pumping capacity pump 1
        controlSignals.add(new ControlSignal("Flowrate pump 2", "l/h", true, false, "This is a specific demo tooltip.")); // Pumping capacity pump 2

        List<Log> logs = new ArrayList<>();
        logs.add(new Log("Flowrate", "l/h"));
        logs.add(new Log("Status"));
        logs.add(new Log("Timestep counter", ""));
        logs.add(new Log("Random value"));
        logs.add(new Log());
        logs.add(null);

        Properties config = new Properties();
        int defaultNumGenericSensors = 2;
        int defaultNumGenericControlSignals = 2;
        int numGenericProperties = 2;
        try {
            File configFile = new File("" + parameters.get("Plugin.DataPath") + File.separator + "config.properties");
            if (configFile.exists()) {
                try(FileReader fileReader = new FileReader(configFile);
                        BufferedReader reader = new BufferedReader(fileReader);) {
                    config.load(reader);
                    Object obj;
                    numGenericProperties = (obj = config.get("numGenericProperties")) != null ? Integer.parseInt(obj.toString()) : 2;
                    defaultNumGenericSensors = (obj = config.get("defaultNumGenericSensors")) != null ? Integer.parseInt(obj.toString()) : 2;
                    defaultNumGenericControlSignals = (obj = config.get("defaultNumGenericControlSignals")) != null ? Integer.parseInt(obj.toString()) : 2;
                    printMessage.add("PluginController '" + getName() + "' configuration:\nnumGenericProperties = " + numGenericProperties + "\nnumGenericSensors = " + defaultNumGenericSensors + "\nnumGenericControlSignals = " + defaultNumGenericControlSignals);
                    sLog.info("Config file found: " + configFile);
                } catch (IOException e) {
                    sLog.warning(e.getMessage());
                }
            } else {
                sLog.info("No config file available: " + configFile);
            }
        } catch (Exception e) {
            sLog.warning(e.getMessage());
        }
        // Definition flow rate setting: Variable value / Fixed value
        properties.add(new Property("Flowrate type", new String[] { "Variable flowrate", "Fixed flowrate" }, 1));
        properties.add(new Property("Fixed flowrate", 10f, 0, 100000, "l/h")); // Fixed flow rate
        properties.add(new Property("Flowrate scale", 1, 0.1f, 1000, "l/h")); // Scaling factor
        properties.add(new Property(PROPERTY_NUM_GENERIC_SENSORS, defaultNumGenericSensors, 0, 100));
        properties.add(new Property(PROPERTY_NUM_GENERIC_CONTROL_SIGNALS, defaultNumGenericControlSignals, 0, 100));

        parameters.put("Plugin.PrintMessage", printMessage);

        return new PluginControllerConfiguration(properties, sensors, controlSignals, logs, numGenericProperties, defaultNumGenericSensors, defaultNumGenericControlSignals, "plugin/images/controller_plugin.png",
                PROPERTY_NUM_GENERIC_SENSORS, PROPERTY_NUM_GENERIC_CONTROL_SIGNALS, null, null);
    }

    @Override
    public int[] control(int simulationTime, boolean status, float[] sensors, float[] controlSignals, float[] logValues,
            boolean preRun, Map<String, Object> parameters) {
        float statusSignal = 0;
        float measuredFlowrate = sensors[0] + (sensorsUsed[1] ? sensors[1] : 0);
        if (status && propertiesInt[0] == DESGIN_FLOWRATE_VARIABLE && sensorsUsed[2]) {
            // Variable flowrate case:
            // If the measured flow rate(s) is/are bigger then the variable
            // flowrate, set status to 1
            statusSignal = measuredFlowrate > sensors[2] ? 1 : 0;
        } else if (status && propertiesInt[0] == DESGIN_FLOWRATE_FIXED) {
            // Fixed flowrate case:
            // If the measured flow rate(s) is/are bigger then the fixed
            // flowrate, set status to 1
            statusSignal = measuredFlowrate > propertiesFloat[1] ? 1 : 0;
        }
        controlSignals[0] = controlSignals[1] = statusSignal;

        if (controlSignalsUsed[2]) {
            // Measured flowrate is scaled to the output, if used
            controlSignals[2] = measuredFlowrate * propertiesFloat[2];
        }
        if (controlSignalsUsed[3]) {
            // Measured flowrate is scaled to the output, if used
            controlSignals[3] = measuredFlowrate * propertiesFloat[2];
        }
        logValues[0] = measuredFlowrate;
        logValues[1] = status ? 1 : 0;
        if (!preRun) {
            logValues[2] = timestepCounter++;
        }
        logValues[3] = (float) Math.random();
        logValues[4] = simulationTime;

        // Set the max timestep to 1min for the first 5min of each hour
        if (simulationTime % 3600 < 240) {
            return new int[] {simulationTime + 120};
        } else {
            return null;
        }
    }

    @Override
    public List<String> getPropertiesToHide(PolysunSettings polysunSettings, Map<String, Object> parameters) {
        List<String> propertiesToHide = super.getPropertiesToHide(polysunSettings, parameters);
        // Show 'Fixed flowrate' only if 'Flowrate type' = 'Fixed'
        if (propertiesInt[0] != DESGIN_FLOWRATE_FIXED) {
            propertiesToHide.add(propertiesName[1]);
        }
        return propertiesToHide;
    }

    @Override
    public void terminateSimulation(Map<String, Object> parameters) {
        super.terminateSimulation(parameters);
        sLog.info(getName() + " total number of timesteps: " + timestepCounter);
    }

    @Override
    public void initialiseSimulation(Map<String, Object> parameters) throws PluginControllerException {
        super.initialiseSimulation(parameters);
        timestepCounter = 0;
    }

    @Override
    public List<String> getSensorsToHide(PolysunSettings polysunSettings, Map<String, Object> parameters) {
        List<String> propertiesToHide = super.getSensorsToHide(polysunSettings, parameters);
        // Show sensor 'Variable flowrate sensor' show if
        // 'Flowrate type' = 'Variable'
        if (propertiesInt[0] != DESGIN_FLOWRATE_VARIABLE) {
            propertiesToHide.add(sensorsName[2]);
        }
        return propertiesToHide;
    }

    @Override
    public List<String> getControlSignalsToHide(PolysunSettings propertyValues, Map<String, Object> parameters) {
        return null;
    }

    @Override
    public String getDescription() {
        return "Controls the status (on/off) of one or two different components and the flowrate of a given pump based"
            + " on two flowrate sensors. Additionally, there are generic properties, inputs and outputs.";
    }

}
