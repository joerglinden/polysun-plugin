/**
 * Package containing the DemoFlowratePluginController.
 * 
 * @see com.velasolaris.plugin.controller.demo.DemoFlowratePluginController
 */
package com.velasolaris.plugin.controller.demo;
