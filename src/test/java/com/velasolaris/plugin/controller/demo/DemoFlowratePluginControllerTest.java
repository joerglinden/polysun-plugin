package com.velasolaris.plugin.controller.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.velasolaris.plugin.controller.spi.AbstractPluginController;
import com.velasolaris.plugin.controller.spi.IPluginController;
import com.velasolaris.plugin.controller.spi.PluginControllerConfiguration;
import com.velasolaris.plugin.controller.spi.PluginControllerConfiguration.AbstractProperty.Type;
import com.velasolaris.plugin.controller.spi.PluginControllerConfiguration.ControlSignal;
import com.velasolaris.plugin.controller.spi.PluginControllerConfiguration.Log;
import com.velasolaris.plugin.controller.spi.PluginControllerConfiguration.Property;
import com.velasolaris.plugin.controller.spi.PluginControllerConfiguration.Sensor;
import com.velasolaris.plugin.controller.spi.PluginControllerException;
import com.velasolaris.plugin.controller.spi.PolysunSettings;
import com.velasolaris.plugin.controller.spi.PolysunSettings.PropertyValue;

/**
 * JUnit tests for the DemoFlowratePluginController.
 *
 * @author rkurmann
 * @since Polysun 9.1
 *
 */
public class DemoFlowratePluginControllerTest {

    private static double PRECISION = 0.000001;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    private IPluginController controller;

    /**
     * Returns a PolysunSetting object as it would be returned by Polysun. This
     * object must correspond to the configuration by
     * {@link DemoFlowratePluginController#getConfiguration(Map)}.
     */
    private PolysunSettings createPolysunSettings(int flowrateType, float fixDesignFlowrate,
            float scaleFlowrate, Boolean measuredFlowrate, boolean measuredFlowrate2, boolean variableDesignFlowrate,
            Boolean statusPump1, Boolean status, Boolean flowratePump1, Boolean flowratePump2) {
        List<PropertyValue> properties = new ArrayList<>();
        // Definition flow rate setting: Variable value / Fixed value
        properties.add(new PropertyValue("Flowrate type", flowrateType, ""));
        properties.add(new PropertyValue("Fixed flowrate", fixDesignFlowrate, "l/h")); // Fixed flow rate
        properties.add(new PropertyValue("Flowrate scale", scaleFlowrate, "l/h")); // Scaling factor
        properties.add(new PropertyValue(DemoFlowratePluginController.PROPERTY_NUM_GENERIC_SENSORS, 2, ""));
        properties.add(new PropertyValue(DemoFlowratePluginController.PROPERTY_NUM_GENERIC_CONTROL_SIGNALS, 2, ""));

        List<Sensor> sensors = new ArrayList<>();
        sensors.add(new Sensor("Flowrate sensor 1", "l/h", true, true, measuredFlowrate)); // Flowrate sensor 1
        sensors.add(new Sensor("Flowrate sensor 2", "l/h", true, false, measuredFlowrate2)); // Flowrate sensor 2
        sensors.add(new Sensor("Variable flowrate sensor", "l/h", true, false, variableDesignFlowrate)); // Variable flow rate

        List<ControlSignal> controlSignals = new ArrayList<>();
        controlSignals.add(new ControlSignal("Signal pump 1", "", false, true, statusPump1)); // On/Off pump 1
        controlSignals.add(new ControlSignal("Signal pump 2", "", false, false, status)); // On/Off pump 2
        controlSignals.add(new ControlSignal("Flowrate pump 1", "l/h", true, false, flowratePump1)); // Pumping capacity pump 1
        controlSignals.add(new ControlSignal("Flowrate pump 2", "l/h", true, false, flowratePump2)); // Pumping capacity pump 2

        List<Log> logs = new ArrayList<>();
        logs.add(new Log("Flowrate", "l/h"));
        logs.add(new Log("Status"));
        logs.add(new Log("Timestep counter", ""));
        logs.add(new Log("Random value"));
        logs.add(new Log());
        logs.add(null);

        return new PolysunSettings(properties, sensors, controlSignals, logs);
    }

    private PolysunSettings createPolysunSettingsDefaultConfiguration() {
        return createPolysunSettings(1, 10, 1, true, false, false, true, false, true, false);
    }

    /**
     * Creates PolysunSettingsObject from configuration.
     */
    private PolysunSettings createPolysunSettingsFromConfiguration() throws PluginControllerException {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("Plugin.DataPath", "");
        PluginControllerConfiguration configuration = controller.getConfiguration(parameters);
        List<PropertyValue> properties = new ArrayList<>();
        for (Property property : configuration.getProperties()) {
            PropertyValue propertyValue = null;
            if (property.getType() == Type.FLOAT) {
                propertyValue = new PropertyValue(property.getName(), property.getDefaultFloat(),
                        property.getUnit());
            } else if (property.getType() == Type.INTEGER) {
                propertyValue = new PropertyValue(property.getName(), property.getDefaultInt(),
                        property.getUnit());
            } else if (property.getType() == Type.STRING) {
                propertyValue = new PropertyValue(property.getName(), property.getDefaultString());
            }
            properties.add(propertyValue);
        }
        List<Sensor> sensors = new ArrayList<>();
        for (Sensor sensor : configuration.getSensors()) {
            sensors.add(new Sensor(sensor.getName(), sensor.getUnit(), sensor.isAnalog(), sensor.isRequired(),
                        "Flowrate sensor 1".equals(sensor.getName())));
        }
        List<ControlSignal> controlSignals = new ArrayList<>();
        for (ControlSignal controlSignal : configuration.getControlSignals()) {
            controlSignals.add(new ControlSignal(controlSignal.getName(), controlSignal.getUnit(),
                        controlSignal.isAnalog(), controlSignal.isRequired(),
                        "Signal pump 1".equals(controlSignal.getName()) || "Flowrate pump 1".equals(controlSignal.getName())));
        }

        List<Log> logs = new ArrayList<>();
        for (Log log : configuration.getLogs()) {
            if (log != null) {
                logs.add(new Log(log.getName(), "".equals(log.getUnit()) && !"Timestep counter".equals(log.getName()) ? null: log.getUnit()));
            } else {
                logs.add(null);
            }
        }

        return new PolysunSettings(properties, sensors, controlSignals, logs);
    }

    @Before
    public void setUp() throws Exception {
        controller = new DemoFlowratePluginController();
    }

    @Test
    public void testControlDefaultSignalOff() throws PluginControllerException {
        controller.build(createPolysunSettingsDefaultConfiguration(), null);
        float[] sensors = new float[] { 5, Float.NaN, Float.NaN };
        float[] controlSignals = new float[4];
        float[] logValues = new float[5];
        int[] timepoints = controller.control(3500, true, sensors, controlSignals, logValues, false, null);
        assertEquals("Wrong number of timepoints", null, timepoints);
        assertEquals("Wrong control signal 1", 0, controlSignals[0], PRECISION);
        assertEquals("Wrong control signal 2", 0, controlSignals[1], PRECISION);
        assertEquals("Wrong control signal 3", 5, controlSignals[2], PRECISION);
    }

    @Test
    public void testControlDefaultSignalOffTimepoint() throws PluginControllerException {
        controller.build(createPolysunSettingsDefaultConfiguration(), null);
        float[] sensors = new float[] { 5, Float.NaN, Float.NaN };
        float[] controlSignals = new float[4];
        float[] logValues = new float[5];
        int[] timepoints = controller.control(3630, true, sensors, controlSignals, logValues, false, null);
        assertEquals("Wrong number of timepoints", 1, timepoints.length);
        assertEquals("Wrong control signal 1", 0, controlSignals[0], PRECISION);
        assertEquals("Wrong control signal 2", 0, controlSignals[1], PRECISION);
        assertEquals("Wrong control signal 3", 5, controlSignals[2], PRECISION);
    }

    @Test
    public void testControlDefaultSignalOn() throws PluginControllerException {
        controller.build(createPolysunSettingsDefaultConfiguration(), null);
        float[] sensors = new float[] { 20, Float.NaN, Float.NaN };
        float[] controlSignals = new float[4];
        float[] logValues = new float[5];
        int[] timepoints = controller.control(3500, true, sensors, controlSignals, logValues, false, null);
        assertEquals("Wrong number of timepoints", null, timepoints);
        assertEquals("Wrong control signal 1", 1, controlSignals[0], PRECISION);
        assertEquals("Wrong control signal 2", 1, controlSignals[1], PRECISION);
        assertEquals("Wrong control signal 3", 20, controlSignals[2], PRECISION);
    }

    @Test
    public void testControlDefaultStatusOff() throws PluginControllerException {
        controller.build(createPolysunSettingsDefaultConfiguration(), null);
        float[] sensors = new float[] { 20, Float.NaN, Float.NaN };
        float[] controlSignals = new float[4];
        float[] logValues = new float[5];
        int[] timepoints = controller.control(3500, false, sensors, controlSignals, logValues, false, null);
        assertEquals("Wrong number of timepoints", null, timepoints);
        assertEquals("Wrong control signal 1", 0, controlSignals[0], PRECISION);
        assertEquals("Wrong control signal 2", 0, controlSignals[1], PRECISION);
        assertEquals("Wrong control signal 3", 20, controlSignals[2], PRECISION);
    }

    @Test
    public void testControlFixFlowrate2SensorsSignalOff() throws PluginControllerException {
        float sensorFlowrate = 2;
        float sensorFlowrate2 = 5;
        float fixDesignFlowrate = 10;
        boolean secondFlowrate = true;
        controller.build(createPolysunSettings(1, fixDesignFlowrate, 1, true, secondFlowrate, false, true, false, true, false),
                null);
        float[] sensors = new float[] { sensorFlowrate, sensorFlowrate2, Float.NaN };
        float[] controlSignals = new float[4];
        float[] logValues = new float[5];

        int[] timepoints = controller.control(3500, true, sensors, controlSignals, logValues, false, null);
        assertEquals("Wrong number of timepoints", null, timepoints);
        assertEquals("Wrong control signal 1", 0, controlSignals[0], PRECISION);
        assertEquals("Wrong control signal 2", 0, controlSignals[1], PRECISION);
        assertEquals("Wrong control signal 3", sensorFlowrate + sensorFlowrate2, controlSignals[2], PRECISION);
    }

    @Test
    public void testControlFixFlowrate2SensorsSignalOn() throws PluginControllerException {
        float sensorFlowrate = 2;
        float sensorFlowrate2 = 15;
        float fixDesignFlowrate = 10;
        boolean secondFlowrate = true;
        controller.build(createPolysunSettings(1, fixDesignFlowrate, 1, true, secondFlowrate, false, true, false, true, false),
                null);
        float[] sensors = new float[] { sensorFlowrate, sensorFlowrate2, Float.NaN };
        float[] controlSignals = new float[4];
        float[] logValues = new float[5];

        int[] timepoints = controller.control(3500, true, sensors, controlSignals, logValues, false, null);
        assertEquals("Wrong number of timepoints", null, timepoints);
        assertEquals("Wrong control signal 1", 1, controlSignals[0], PRECISION);
        assertEquals("Wrong control signal 2", 1, controlSignals[1], PRECISION);
        assertEquals("Wrong control signal 3", sensorFlowrate + sensorFlowrate2, controlSignals[2], PRECISION);
    }

    @Test
    public void testControlFixFlowrateSignalOff() throws PluginControllerException {
        float sensorFlowrate = 0;
        float fixDesignFlowrate = 10;
        controller.build(createPolysunSettings(1, fixDesignFlowrate, 1, true, false, false, true, false, true, false), null);
        float[] sensors = new float[] { sensorFlowrate, Float.NaN, Float.NaN };
        float[] controlSignals = new float[4];
        float[] logValues = new float[5];

        int[] timepoints = controller.control(3500, true, sensors, controlSignals, logValues, false, null);
        assertEquals("Wrong number of timepoints", null, timepoints);
        assertEquals("Wrong control signal 1", 0, controlSignals[0], PRECISION);
        assertEquals("Wrong control signal 2", 0, controlSignals[1], PRECISION);
        assertEquals("Wrong control signal 3", sensorFlowrate, controlSignals[2], PRECISION);
    }

    @Test
    public void testControlFixFlowrateSignalOn() throws PluginControllerException {
        float sensorFlowrate = 50;
        float fixDesignFlowrate = 10;
        controller.build(createPolysunSettings(1, fixDesignFlowrate, 1, true, false, false, true, false, true, false), null);
        float[] sensors = new float[] { sensorFlowrate, Float.NaN, Float.NaN };
        float[] controlSignals = new float[4];
        float[] logValues = new float[5];

        int[] timepoints = controller.control(3500, true, sensors, controlSignals, logValues, false, null);
        assertEquals("Wrong number of timepoints", null, timepoints);
        assertEquals("Wrong control signal 1", 1, controlSignals[0], PRECISION);
        assertEquals("Wrong control signal 2", 1, controlSignals[1], PRECISION);
        assertEquals("Wrong control signal 3", sensorFlowrate, controlSignals[2], PRECISION);
    }

    @Test
    public void testControlFromConfigurationSignalOff() throws PluginControllerException {
        controller.build(createPolysunSettingsFromConfiguration(), null);
        float[] sensors = new float[] { 5, Float.NaN, Float.NaN };
        float[] controlSignals = new float[4];
        float[] logValues = new float[5];
        int[] timepoints = controller.control(3500, true, sensors, controlSignals, logValues, false, null);
        assertEquals("Wrong number of timepoints", null, timepoints);
        assertEquals("Wrong control signal 1", 0, controlSignals[0], PRECISION);
        assertEquals("Wrong control signal 2", 0, controlSignals[1], PRECISION);
        assertEquals("Wrong control signal 3", 5, controlSignals[2], PRECISION);
    }

    @Test
    public void testControlFromConfigurationSignalOn() throws PluginControllerException {
        controller.build(createPolysunSettingsFromConfiguration(), null);
        float[] sensors = new float[] { 20, Float.NaN, Float.NaN };
        float[] controlSignals = new float[4];
        float[] logValues = new float[5];
        int[] timepoints = controller.control(3500, true, sensors, controlSignals, logValues, false, null);
        assertEquals("Wrong number of timepoints", null, timepoints);
        assertEquals("Wrong control signal 1", 1, controlSignals[0], PRECISION);
        assertEquals("Wrong control signal 2", 1, controlSignals[1], PRECISION);
        assertEquals("Wrong control signal 3", 20, controlSignals[2], PRECISION);
    }

    @Test
    public void testControlVariableFlowrateSensorNotUsedSignalOff() throws PluginControllerException {
        float sensorFlowrate = 150;
        float variableDesignFlowrate = 100;
        boolean variableFlowrateSet = false;
        controller.build(createPolysunSettings(0, 10, 1, true, false, variableFlowrateSet, true, false, true, false), null);
        float[] sensors = new float[] { sensorFlowrate, Float.NaN, variableDesignFlowrate };
        float[] controlSignals = new float[4];
        float[] logValues = new float[5];

        int[] timepoints = controller.control(3500, true, sensors, controlSignals, logValues, false, null);
        assertEquals("Wrong number of timepoints", null, timepoints);
        assertEquals("Wrong control signal 1", 0, controlSignals[0], PRECISION);
        assertEquals("Wrong control signal 2", 0, controlSignals[1], PRECISION);
        assertEquals("Wrong control signal 3", sensorFlowrate, controlSignals[2], PRECISION);
    }

    @Test
    public void testControlVariableFlowrateSignalOff() throws PluginControllerException {
        float sensorFlowrate = 50;
        float variableDesignFlowrate = 100;
        boolean variableFlowrateSet = true;
        controller.build(createPolysunSettings(0, 10, 1, true, false, variableFlowrateSet, true, false, true, false), null);
        float[] sensors = new float[] { sensorFlowrate, Float.NaN, variableDesignFlowrate };
        float[] controlSignals = new float[4];
        float[] logValues = new float[5];

        int[] timepoints = controller.control(3500, true, sensors, controlSignals, logValues, false, null);
        assertEquals("Wrong number of timepoints", null, timepoints);
        assertEquals("Wrong control signal 1", 0, controlSignals[0], PRECISION);
        assertEquals("Wrong control signal 2", 0, controlSignals[1], PRECISION);
        assertEquals("Wrong control signal 3", sensorFlowrate, controlSignals[2], PRECISION);
    }

    @Test
    public void testControlVariableFlowrateSignalOn() throws PluginControllerException {
        float sensorFlowrate = 150;
        float variableDesignFlowrate = 100;
        boolean variableFlowrateSet = true;
        controller.build(createPolysunSettings(0, 10, 1, true, false, variableFlowrateSet, true, false, true, false), null);
        float[] sensors = new float[] { sensorFlowrate, Float.NaN, variableDesignFlowrate };
        float[] controlSignals = new float[4];
        float[] logValues = new float[5];

        int[] timepoints = controller.control(3500, true, sensors, controlSignals, logValues, false, null);
        assertEquals("Wrong number of timepoints", null, timepoints);
        assertEquals("Wrong control signal 1", 1, controlSignals[0], PRECISION);
        assertEquals("Wrong control signal 2", 1, controlSignals[1], PRECISION);
        assertEquals("Wrong control signal 3", sensorFlowrate, controlSignals[2], PRECISION);
    }

    @Test
    public void testControlVariableFlowrateTwoSensorsSignalOff() throws PluginControllerException {
        float sensorFlowrate = 50;
        float variableDesignFlowrate = 100;
        boolean variableFlowrateSet = true;
        float sensorFlowrate2 = 15;
        boolean secondFlowrate = true;
        controller.build(createPolysunSettings(0, 10, 1, true, secondFlowrate, variableFlowrateSet, true, false, true, false),
                null);
        float[] sensors = new float[] { sensorFlowrate, sensorFlowrate2, variableDesignFlowrate };
        float[] controlSignals = new float[4];
        float[] logValues = new float[5];

        int[] timepoints = controller.control(3500, true, sensors, controlSignals, logValues, false, null);
        assertEquals("Wrong number of timepoints", null, timepoints);
        assertEquals("Wrong control signal 1", 0, controlSignals[0], PRECISION);
        assertEquals("Wrong control signal 2", 0, controlSignals[1], PRECISION);
        assertEquals("Wrong control signal 3", sensorFlowrate + sensorFlowrate2, controlSignals[2], PRECISION);
    }

    @Test
    public void testControlVariableFlowrateTwoSensorsSignalOn() throws PluginControllerException {
        float sensorFlowrate = 50;
        float variableDesignFlowrate = 100;
        boolean variableFlowrateSet = true;
        float sensorFlowrate2 = 60;
        boolean secondFlowrate = true;
        controller.build(createPolysunSettings(0, 10, 1, true, secondFlowrate, variableFlowrateSet, true, false, true, false),
                null);
        float[] sensors = new float[] { sensorFlowrate, sensorFlowrate2, variableDesignFlowrate };
        float[] controlSignals = new float[4];
        float[] logValues = new float[5];

        int[] timepoints = controller.control(3500, true, sensors, controlSignals, logValues, false, null);
        assertEquals("Wrong number of timepoints", null, timepoints);
        assertEquals("Wrong control signal 1", 1, controlSignals[0], PRECISION);
        assertEquals("Wrong control signal 2", 1, controlSignals[1], PRECISION);
        assertEquals("Wrong control signal 3", sensorFlowrate + sensorFlowrate2, controlSignals[2], PRECISION);
    }

    @Test
    public void testControlVariableFlowrateTwoSensorsScalingSignalOn() throws PluginControllerException {
        float sensorFlowrate = 50;
        float variableDesignFlowrate = 100;
        boolean variableFlowrateSet = true;
        float sensorFlowrate2 = 60;
        boolean secondFlowrate = true;
        float scaling = 0.5f;
        controller.build(
                createPolysunSettings(0, 10, scaling, true, secondFlowrate, variableFlowrateSet, true, false, true, false),
                null);
        float[] sensors = new float[] { sensorFlowrate, sensorFlowrate2, variableDesignFlowrate };
        float[] controlSignals = new float[4];
        float[] logValues = new float[5];

        int[] timepoints = controller.control(3500, true, sensors, controlSignals, logValues, false, null);
        assertEquals("Wrong number of timepoints", null, timepoints);
        assertEquals("Wrong control signal 1", 1, controlSignals[0], PRECISION);
        assertEquals("Wrong control signal 2", 1, controlSignals[1], PRECISION);
        assertEquals("Wrong control signal 3", (sensorFlowrate + sensorFlowrate2) * scaling, controlSignals[2],
                PRECISION);
    }

    @Test
    public void testControlVariableFlowrateTwoSensorsScalingStatusOff() throws PluginControllerException {
        float sensorFlowrate = 50;
        float variableDesignFlowrate = 100;
        boolean variableFlowrateSet = true;
        float sensorFlowrate2 = 60;
        boolean secondFlowrate = true;
        float scaling = 0.5f;
        controller.build(
                createPolysunSettings(0, 10, scaling, true, secondFlowrate, variableFlowrateSet, true, false, true, false),
                null);
        float[] sensors = new float[] { sensorFlowrate, sensorFlowrate2, variableDesignFlowrate };
        float[] controlSignals = new float[4];
        float[] logValues = new float[5];

        int[] timepoints = controller.control(3500, false, sensors, controlSignals, logValues, false, null);
        assertEquals("Wrong number of timepoints", null, timepoints);
        assertEquals("Wrong control signal 1", 0, controlSignals[0], PRECISION);
        assertEquals("Wrong control signal 2", 0, controlSignals[1], PRECISION);
        assertEquals("Wrong control signal 3", (sensorFlowrate + sensorFlowrate2) * scaling, controlSignals[2],
                PRECISION);
    }

    // @Test
    public void testFail() {
        fail("Test FAIL");
    }

    @Test
    public void testGetConfiguration() throws PluginControllerException {
        PluginControllerConfiguration configuration = controller.getConfiguration(new HashMap<String, Object>());
        assertEquals("Wrong number of configured properties", 5, configuration.getProperties().size());
        assertEquals("Wrong number of generic properties", 2, configuration.getNumGenericProperties());
        assertEquals("Wrong number of configured sensors", 3, configuration.getSensors().size());
        assertEquals("Wrong number of generic sensors", 2, configuration.getNumGenericSensors());
        assertEquals("Wrong number of configured controlSignals", 4, configuration.getControlSignals().size());
        assertEquals("Wrong number of generic controlSignals", 2, configuration.getNumGenericControlSignals());
        assertEquals("Wrong number of logs", 6, configuration.getLogs().size());
        assertEquals("Wrong controller image", "plugin/images/controller_plugin.png", configuration.getImagePath());

        assertEquals("PolysunSetting sensors different", createPolysunSettingsDefaultConfiguration().getSensors(),
                createPolysunSettingsFromConfiguration().getSensors());
        assertEquals("PolysunSetting control signals different",
                createPolysunSettingsDefaultConfiguration().getControlSignals(),
                createPolysunSettingsFromConfiguration().getControlSignals());
        assertEquals("PolysunSetting property values different",
                createPolysunSettingsDefaultConfiguration().getPropertyValues(),
                createPolysunSettingsFromConfiguration().getPropertyValues());
        assertEquals("PolysunSetting log values different",
                createPolysunSettingsDefaultConfiguration().getLogs(),
                createPolysunSettingsFromConfiguration().getLogs());
        assertEquals("PolysunSetting configuration different", createPolysunSettingsDefaultConfiguration(),
                createPolysunSettingsFromConfiguration());
    }

    @Test
    public void testGetControlSignalsToHide() {
        int state = 1;
        List<String> controlSignalsToHide = controller.getControlSignalsToHide(
                createPolysunSettings(state, 10, 1, true, false, false, true, false, true, false), null);
        assertEquals("No signals to hide expected", null, controlSignalsToHide);
        state = 0;
        controlSignalsToHide = controller.getControlSignalsToHide(
                createPolysunSettings(state, 10, 1, true, false, false, true, false, true, false), null);
        assertEquals("No signals to hide expected", null, controlSignalsToHide);
    }

    @Test
    public void testGetName() {
        assertEquals("DemoFlowrate", controller.getName());
    }

    @Test
    public void testGetPropertiesToHide() {
        int state = 1;
        List<String> propertiesToHide = controller
            .getPropertiesToHide(createPolysunSettings(state, 10, 1, true, false, false, true, false, true, false), null);
        assertEquals("Wrong number of propreties to hide", 0, propertiesToHide.size());
        state = 0;
        propertiesToHide = controller
            .getPropertiesToHide(createPolysunSettings(state, 10, 1, true, false, false, true, false, true, false), null);
        assertEquals("Wrong number of propreties to hide", 1, propertiesToHide.size());
        assertEquals("Fixed flowrate", propertiesToHide.get(0));
    }

    @Test
    public void testGetSensorsToHide() {
        int state = 1;
        List<String> sensorsToHide = controller
            .getSensorsToHide(createPolysunSettings(state, 10, 1, true, false, false, true, false, true, false), null);
        assertEquals("Wrong number of sensors to hide", 1, sensorsToHide.size());
        assertEquals("Variable flowrate sensor", sensorsToHide.get(0));
        state = 0;
        sensorsToHide = controller
            .getSensorsToHide(createPolysunSettings(state, 10, 1, true, false, false, true, false, true, false), null);
        assertEquals("No sensors to hide expected", 0, sensorsToHide.size());
    }

    @Test
    public void testPolysunSettingsGetter() throws PluginControllerException {
        controller.build(createPolysunSettingsDefaultConfiguration(), null);
        assertEquals("Wrong property name returned", "Flowrate type",
                ((AbstractPluginController) controller).getProperty("Flowrate type").getName());
        assertEquals("Wrong sensor name returned", "Flowrate sensor 1",
                ((AbstractPluginController) controller).getSensor("Flowrate sensor 1").getName());
        assertEquals("Wrong control signal name returned", "Signal pump 1",
                ((AbstractPluginController) controller).getControlSignal("Signal pump 1").getName());

        assertEquals("Inexistent property failed", null, ((AbstractPluginController) controller).getProperty("XXX"));
        assertEquals("Inexistent property failed", null, ((AbstractPluginController) controller).getSensor("XXX"));
        assertEquals("Inexistent property failed", null,
                ((AbstractPluginController) controller).getControlSignal("XXX"));

        assertEquals("Wrong sensor index returned", 0,
                ((AbstractPluginController) controller).getPropertyIndex("Flowrate type"));
        assertEquals("Wrong sensor index returned", 2,
                ((AbstractPluginController) controller).getPropertyIndex("Flowrate scale"));
        assertEquals("Wrong sensor index returned", 0,
                ((AbstractPluginController) controller).getSensorIndex("Flowrate sensor 1"));
        assertEquals("Wrong sensor index returned", 2,
                ((AbstractPluginController) controller).getSensorIndex("Variable flowrate sensor"));
        assertEquals("Wrong control signal index returned", 0,
                ((AbstractPluginController) controller).getControlSignalIndex("Signal pump 1"));
        assertEquals("Wrong control signal index returned", 2,
                ((AbstractPluginController) controller).getControlSignalIndex("Flowrate pump 1"));

        assertEquals("Wrong sensor index returned", -1, ((AbstractPluginController) controller).getSensorIndex("XXX"));
        assertEquals("Wrong control signal index returned", -1,
                ((AbstractPluginController) controller).getControlSignalIndex("XXX"));
    }


}
